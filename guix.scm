;; deep-nebula -- Deep nebula theme for emacs.
;; Copyright (C) 2018  Pierre-Antoine Rouby <contact@parouby.fr>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules ((guix licenses) #:prefix license:)
             (guix gexp)
             (guix packages)
             (guix git-download)
             (guix build-system emacs)
             (gnu packages))

(package
  (name "emacs-deep-nebula-theme")
  (version "0.0.2")
  (source (local-file "." "emacs-deep-nebula-theme"
                      #:recursive? #t))
  (build-system emacs-build-system)
  (home-page "https://framagit.org/prouby/emacs-deep-nebula")
  (synopsis "Deep nebula theme for emacs")
  (description "This package provide deep nebula theme for emacs.")
  (license license:gpl3+))
