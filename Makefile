## Makefile --- deep-nebula Makefile.             -*- lexical-binding: t; -*-

# Copyright (C) 2024  Pierre-Antoine Rouby

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

GZ = gzip

EMACS = emacs -Q -q --batch -nw
EMACS_COMPILE = -f emacs-lisp-byte-compile
EMACS_NATIVE_COMPILE = -f emacs-lisp-native-compile
EMACS_DIR = ~/.emacs.d/themes/

SOURCES = deep-nebula-theme.el
COPRESSED_FILE += $(SOURCES:.el=.el.gz)
COMPILED_FILE += $(SOURCES:.el=.elc)
NATIVE_COMPILED_FILE += $(SOURCES:.el=.eln)

all: compile

compile: $(SOURCES) $(COMPILED_FILE) $(COPRESSED_FILE) $(NATIVE_COMPILED_FILE)

%.el.gz: %.el
	$(info GZ           $@)
	@$(GZ) -k $<

%.elc: %.el
	$(info BYTE         $@)
	@$(EMACS) $< $(EMACS_COMPILE)

%.eln: %.el
	$(info NATIVE       $@)
	@$(EMACS) $< $(EMACS_NATIVE_COMPILE)

install:
	$(info INSTALL)
	@mkdir -p $(EMACS_DIR)
	@cp ./$(SOURCES)       $(EMACS_DIR)$(SOURCES)
	@cp ./$(COMPILED_FILE) $(EMACS_DIR)$(COMPILED_FILE)

clean:
	$(info CLEAN)
	@rm -v *.elc
	@rm -v *.eln
	@rm -v *.el.gz
