;;; deep-nebula-theme.el --- Custom face theme for Emacs  -*-coding: utf-8 -*-
;;
;; Copyright (C) 2018-2021 Pierre-Antoine Rouby <contact@parouby.fr>.
;;
;; deep-nebula-theme is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; deep-nebula-theme is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

(deftheme deep-nebula
  "Deep theme for Emacs.")

(let* ((class '((class color) (min-colors 89)))
       (deep-white       "#f8f8f8")
       (deep-grey        "#808080")
       (deep-lightgrey   "#d3d3d3")
       (deep-lightgrey-2 "#cccccc")
       (deep-lightgrey-3 "#b6b6b6")
       (deep-black       "#000000")
       (deep-darkgrey    "#281d38")
       (deep-darkgrey-2  "#131313")
       (deep-darkgrey-3  "#363742")
       (deep-darkgrey-4  "#464751")
       (deep-darkgrey-5  "#44475a")
       (deep-purple      "#a766fe")
       (deep-purple-2    "#5c388d")
       (deep-purple-3    "#261739")
       (deep-purple-4    "#d099fe")
       (deep-lightblue   "#70a0f2")
       (deep-lightblue-2 "#b7d7f2")
       (deep-blue        "#6264b4")
       (deep-blue-2      "#867dfd")
       (deep-blue-3      "#5f509f")
       (deep-blue-4      "#cfcae3")
       (deep-red         "#ff7777")
       (deep-green       "#a9ec97")
       (deep-green-2     "#baf9aa")
       (deep-yellow      "#fae0a0")
       (deep-orange      "#ffb86c")

       (fg1               deep-lightgrey)
       (bg1               deep-black)
       (bg2               deep-darkgrey)
       (hi1               deep-lightgrey)
       (builtin           deep-lightblue)
       (keyword           deep-purple-4)
       (const             deep-lightblue)
       (comment           deep-grey)
       (func              deep-red)
       (str               deep-yellow)
       (doc               deep-red)
       (type              deep-green-2)
       (var               deep-lightblue)
       (warning           deep-orange)
       (comment-delimiter deep-grey))

  (custom-theme-set-faces
   'deep-nebula

   `(default ((,class (:background ,bg1 :foreground ,fg1))))
   `(cursor  ((,class (:background ,fg1))))

   ;; Highlighting.
   `(fringe              ((,class (:background ,bg1))))
   `(highlight           ((,class (:background ,type
                                               :foreground ,bg1
                                               :underline t))))
   `(region              ((,class (:background ,deep-purple   :foreground ,fg1))))
   `(secondary-selection ((,class (:background ,type  :foreground ,fg1))))
   `(isearch             ((,class (:background ,deep-purple
                                               :foreground ,fg1))))
   `(lazy-highlight      ((,class (:background ,fg1   :foreground ,bg1))))

   ;; Mode line.
   `(mode-line           ((,class (:background ,deep-blue
                                               :foreground ,bg1))))
   `(mode-line-inactive  ((,class (:background ,bg2
                                               :foreground ,fg1))))

   ;; Escape and prompt.
   `(minibuffer-prompt   ((,class (:foreground ,deep-purple :bold t))))
   `(escape-glyph        ((,class (:foreground ,warning :weight bold))))

   ;; Font lock.
   `(font-lock-builtin-face  ((,class (:foreground ,builtin))))
   `(font-lock-comment-face  ((,class (:foreground ,comment
                                                   :italic t))))
   `(font-lock-constant-face ((,class (:foreground ,const :bolt t))))
   `(font-lock-doc-face      ((,class (:foreground ,doc))))
   `(font-lock-function-name-face ((,class (:foreground ,func
				                        :bold t))))
   `(font-lock-keyword-face       ((,class (:foreground ,keyword
				                        :bold nil))))
   `(font-lock-negation-char-face ((,class (:foreground ,deep-blue))))
   `(font-lock-reference-face     ((,class (:foreground ,str))))
   `(font-lock-string-face        ((,class (:foreground ,str))))
   `(font-lock-type-face          ((,class (:foreground ,type))))
   `(font-lock-variable-name-face ((,class (:foreground ,var :bold nil))))
   `(font-lock-warning-face       ((,class (:foreground ,warning :bold t))))
   `(font-lock-comment-delimiter-face ((,class (:foreground
						,comment-delimiter
                                                :italic t))))

   ;; Button and link.
   `(link         ((,class (:foreground ,type :underline t))))
   `(link-visited ((,class (:foreground ,func :underline t))))
   `(button       ((,class (:background ,bg1 :foreground ,fg1))))
   `(header-line  ((,class (:background ,bg1 :foreground ,fg1))))

   ;; Gnus.
   `(gnus-group-news-1      ((,class (:weight bold :foreground ,str))))
   `(gnus-group-news-1-low  ((,class (:foreground ,str))))
   `(gnus-group-news-2      ((,class (:weight bold :foreground ,str))))
   `(gnus-group-news-2-low  ((,class (:foreground ,str))))
   `(gnus-group-news-3      ((,class (:weight bold :foreground ,warning))))
   `(gnus-group-news-3-low  ((,class (:foreground ,warning))))
   `(gnus-group-news-4      ((,class (:weight bold :foreground ,builtin))))
   `(gnus-group-news-4-low  ((,class (:foreground ,builtin))))
   `(gnus-group-news-5      ((,class (:weight bold :foreground ,str))))
   `(gnus-group-news-5-low  ((,class (:foreground ,str))))
   `(gnus-group-news-low    ((,class (:foreground ,builtin))))
   `(gnus-group-mail-1      ((,class (:weight bold :foreground ,str))))
   `(gnus-group-mail-1-low  ((,class (:foreground ,str))))
   `(gnus-group-mail-2      ((,class (:weight bold :foreground ,str))))
   `(gnus-group-mail-2-low  ((,class (:foreground ,str))))
   `(gnus-group-mail-3      ((,class (:weight bold :foreground ,warning))))
   `(gnus-group-mail-3-low  ((,class (:foreground ,warning))))
   `(gnus-group-mail-low    ((,class (:foreground ,builtin))))
   `(gnus-header-content    ((,class (:foreground ,type))))
   `(gnus-header-from       ((,class (:weight bold :foreground ,str))))
   `(gnus-header-subject    ((,class (:foreground ,str))))
   `(gnus-header-name       ((,class (:foreground ,type))))
   `(gnus-header-newsgroups ((,class (:foreground ,str))))

   ;; Message.
   `(message-header-name    ((,class (:foreground ,type :weight bold))))
   `(message-header-cc      ((,class (:foreground ,str))))
   `(message-header-other   ((,class (:foreground ,str))))
   `(message-header-subject ((,class (:foreground ,str))))
   `(message-header-to      ((,class (:foreground ,str))))
   `(message-cited-text     ((,class (:foreground ,builtin))))
   `(message-separator      ((,class (:foreground ,func :weight bold))))

   ;; Helm
   `(helm-selection ((,class (:background ,deep-purple))))

   ;; Ansi-term
   `(term-color-black ((,class (:foreground ,fg1
                                            :background ,bg1))))
   `(term-color-red ((,class (:foreground ,deep-red :background ,bg1))))
   `(term-color-green ((,class (:foreground ,deep-green :background ,bg1))))
   `(term-color-yellow ((,class (:foreground ,deep-orange :background ,bg1))))
   `(term-color-blue ((,class (:foreground ,deep-blue :background ,bg1))))
   `(term-color-magenta ((,class (:foreground ,deep-purple))))
   `(term-color-cyan ((,class (:foreground ,deep-lightblue))))
   `(term-color-white ((,class (:foreground ,deep-white :background ,bg1))))
   `(term ((,class (:foreground ,fg1 :background ,bg1))))

   ;; whitespace-mode
   `(whitespace-space ((,class (:foreground ,deep-red))))
   `(whitespace-hspace ((,class (:foreground ,deep-red))))
   `(whitespace-tab ((,class (:foreground ,deep-red))))
   `(whitespace-newline ((,class (:foreground ,deep-red))))
   `(whitespace-trailing ((,class (:foreground ,deep-red ))))
   `(whitespace-line ((,class (:foreground ,deep-red))))
   `(whitespace-space-before-tab ((,class (:foreground ,deep-red))))
   `(whitespace-indentation ((,class (:foreground ,deep-red))))
   `(whitespace-empty ((,class (:foreground ,deep-red))))
   `(whitespace-space-after-tab ((,class (:foreground ,deep-red))))

   ;; Company-mode
   `(company-tooltip ((,class (:background ,deep-purple-3 :foreground ,fg1))))
   `(company-tooltip-common ((,class (:inherit company-tooltip :foreground ,deep-red))))
   `(company-tooltip-common-selection ((,class (:inherit company-tooltip-selection :foreground ,deep-red))))
   `(company-tooltip-selection ((,class (:foreground ,deep-black :background ,deep-blue-2))))
   `(company-tooltip-annotation ((,class (:inherit company-tooltip :foreground ,deep-black))))
   `(company-scrollbar-fg ((,class (:background ,deep-purple))))
   `(company-scrollbar-bg ((,class (:background ,deep-grey))))
   `(company-preview ((,class (:foreground ,fg1 :background ,deep-purple))))
   `(company-preview-common ((,class (:foreground ,fg1 :background ,deep-purple))))

   ;; Org-mode.
   `(org-level-1 ((,class (:foreground ,deep-purple    :bold t))))
   `(org-level-2 ((,class (:foreground ,deep-blue-2    :bold t))))
   `(org-level-3 ((,class (:foreground ,deep-lightblue :bold t))))
   `(org-level-4 ((,class (:foreground ,deep-purple-2  :bold t))))
   `(org-level-5 ((,class (:foreground ,deep-purple    :bold nil))))
   `(org-level-6 ((,class (:foreground ,deep-blue-2    :bold nil))))
   `(org-level-7 ((,class (:foreground ,deep-lightblue :bold nil))))
   `(org-level-8 ((,class (:foreground ,deep-purple-2  :bold nil))))
   `(org-todo    ((,class (:foreground ,deep-red       :bold t))))
   `(org-done    ((,class (:foreground ,deep-yellow    :bold t))))
   `(org-tag     ((,class (:foreground ,deep-purple    :bold nil))))
   `(org-document-info  ((,class (:foreground ,deep-purple :bold t))))
   `(org-document-title ((,class (:foreground ,deep-purple :bold t))))
   `(org-document-info-keyword ((,class (:foreground ,deep-lightgrey
                                                     :bold nil))))
   `(org-code             ((,class (:foreground ,deep-lightgrey :bold nil))))
   `(org-block            ((,class (:foreground ,fg1
                                                :background ,bg1))))
   `(org-date             ((,class (:foreground ,deep-blue-2     :bold nil))))
   `(org-special-keyword  ((,class (:foreground ,keyword :bold nil))))
   `(org-block-begin-line ((,class (:foreground ,deep-lightgrey-3
                                                :bold nil
                                                :italic t))))
   `(org-block-end-line   ((,class (:foreground ,deep-lightgrey-3
                                                :bold nil
                                                :italic t))))
   `(org-meta-line        ((,class (:foreground ,deep-lightgrey  :bold nil))))
   `(org-sexp-date        ((,class (:foreground ,deep-blue-2     :bold nil))))
   `(org-checkbox ((,class (:background ,deep-purple-3 :foreground ,fg1
                                        :box (:line-width 1 :style released-button)))))))


;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
                  (file-name-as-directory
                   (file-name-directory load-file-name))))

(provide-theme 'deep-nebula)

;;; deep-nebula-theme.el ends here.
